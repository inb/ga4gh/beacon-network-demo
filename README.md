# BSC Beacon Network demo

## Pre-requisites

You need to clone this repository. Then, you need to have installed both `docker` and `docker-compose`. Instructions to install them for [Ubuntu](https://docs.docker.com/install/linux/docker-ce/ubuntu/), [Centos](https://docs.docker.com/install/linux/docker-ce/centos/) and other systems are available at [Docker documentation](https://docs.docker.com/) site.

## Building the docker images

As all the images are properly declared inside [`beacon-network-demo.yml`](beacon-network-demo.yml), you only have to run

```bash
docker-compose build
```

## Registry and aggregator docker images customization on instantiation.

The images publish three TCP ports, 8080, 8443 and 8009 for HTTP, HTTPS and AJP.

The behavior of the images is controlled by the next environment variables. Paths in these variables are internal to the docker instance, so the corresponding volume mappings must be done beforehand (for instance, in a docker-compose.yml or with '-v' declarations):

### Registry

- `BEACON_PRODUCTION`: If set, the registry is started in production mode.

- `BEACON_REGISTRY_DB`: The path to the H2 database where registered beacons are recorded. If it does not exist, it is created on the first run.

- `BEACON_REGISTRY_KEYSTORE`: The Java keystore in JKS format, which must have the public and private keys for the registry, as well as the certification authority chain.

- `BEACON_REGISTRY_KEYSTORE_PASS`: The password of the previous keystore.

- `BEACON_REGISTRY_KEYSTORE_KEY_PASS`: The password of the registry private key in the previous keystore.

- `BEACON_REGISTRY_TRUSTSTORE`: The Java truststore in JKS format, which must have the public keys of the accepted clients for the registry controlled methods.

- `BEACON_REGISTRY_TRUSTSTORE_PASS`: The password of the truststore.

- `BEACON_REGISTRY_INITIAL_BEACONS`: The list of initial beacons to feed the registry on start.

- `BEACON_REGISTRY_CA_FILE`: _(deprecated)_ The certification authority chain, in PEM format.

- `BEACON_REGISTRY_CLIENT_CERT`: _(deprecated)_ The public key certificate of the registry, in PEM format.

- `BEACON_REGISTRY_CLIENT_KEY`: _(deprecated)_ The private decrypted key certificate of the registry.

### Aggregator

- `BEACON_PRODUCTION`: If set, the registry is started in production mode.

- `BEACON_AGGREGATOR_KEYSTORE`: The Java keystore in JKS format, which must have the public and private keys for the aggregator, as well as the certification authority chain.

- `BEACON_AGGREGATOR_KEYSTORE_PASS`: The password of the previous keystore.

- `BEACON_AGGREGATOR_KEYSTORE_KEY_PASS`: The password of the aggregator private key in the previous keystore.

- `BEACON_AGGREGATOR_TRUSTSTORE`: The Java truststore in JKS format, which must have the public keys of the accepted clients for the aggregator controlled methods.

- `BEACON_AGGREGATOR_TRUSTSTORE_PASS`: The password of the the truststore.

## Generating self-signed certificates

The third container, `selfkeygen`, is focused on generating self-signed certificates. For instance, if you want to generate a certificate authority for domain `bn.com`, and two certificates for `r1.bn.com` (a registry) and `ba1.bn.com` (an aggregator related to the registry), with the password `changeme` for the keystores and truststores, next command must be run:

```
docker-compose run -e OUTER_UID="$(id -u)" -e OUTER_GID="$(id -g)" selfkeygen keygenSU.sh /tmp/destdir bn.com changeme r1=ba1,r1 ba1=ba1
```

The certificates will be placed at `bn.com` subdirectory at [security](security) directory. The r1 keystore will have both the ba1 and r1 client certificates (it trusts ba1 and itself), meanwhile the ba1 keystore will only have ba1 client certificates (it only trusts itself).

Certificates for a more complex scenario, like two registries, three beacon aggregators and five clients, would be obtained with next command.

```
docker-compose run -e OUTER_UID="$(id -u)" -e OUTER_GID="$(id -g)" selfkeygen keygenSU.sh /tmp/destdir other-bn.com changeme r1=ba1,ba2,r1 r2=ba2,ba3,r2 ba1=ba1,c2,c3 ba2=ba2,c5 ba3=ba1,ba3,c4,c2,c1 c1 c2 c3 c4 c5
```

## Running and testing

In order to run the demo, after being generated the certificates using the first example, you have to start the beacon network demo:

```bash
docker-compose up -d
```

Once it is running, you only have to execute next commands from inside the `testnode` instance. You can connect to it with next command:

```bash
docker-compose exec testnode /bin/sh
```

and these commands in the new terminal demonstrate that the certificates are working

```bash
# Checking the registry
curl --cacert /etc/beacon-certs/cacert.pem -v -X GET https://r1.bn.com:8443/beacon-network/registry/v1.1.0/

# Checking the aggregator
curl --cacert /etc/beacon-certs/cacert.pem -v -X GET https://ba1.bn.com:8443/beacon-network/aggregator/v1.1.0/

# This one should work
curl --cacert /etc/beacon-certs/cacert.pem --cert /etc/beacon-certs/r1/cert.pem --key /etc/beacon-certs/r1/key.pem -v -X POST -H "Content-Type: application/json" https://r1.bn.com:8443/beacon-network/registry/v1.1.0/services -d @/tmp/beacons.json

# This one should fail
curl --cacert /etc/beacon-certs/cacert.pem -v -X POST -H "Content-Type: application/json" https://r1.bn.com:8443/beacon-network/registry/v1.1.0/services -d @/tmp/beacons.json

```

It also works from outside, but certificate check on `curl` is disabled (it is being assumed you have `curl`in your host):

```bash
# Checking the registry
curl -k -v -X GET https://127.0.0.1:8443/beacon-network/registry/v1.1.0/

# Checking the aggregator
curl -k -v -X GET https://127.0.0.1:18443/beacon-network/aggregator/v1.1.0/

# This one should work
curl -k --cert security/bn.com/r1/cert.pem --key security/bn.com/r1/key.pem -v -X POST -H "Content-Type: application/json" https://127.0.0.1:8443/beacon-network/registry/v1.1.0/services -d @beacons.json

# This one should fail
curl -k -v -X POST -H "Content-Type: application/json" https://127.0.0.1:8443/beacon-network/registry/v1.1.0/services -d @beacons.json

```

## Installing the demo behind a proxy

If you want to have the components integrated into this [`beacon-network-demo.yml`](beacon-network-demo.yml) running behind a reverse proxy managed by Apache, you have to create a couple of virtual hosts, as well as delegate part of the SSL client authentication work on Apache, following next steps (we are also supposing it is running in localhost):

1. Create a directory for the certificates to be used by the registry, as well as by the aggregator. Then, the generated certificates should be copied here. For instance, if you are using the self-signed certificates and you declare the certificate dirs inside Apache, the instructions would be:

  ```bash
  mkdir -p /etc/apache2/certs/registry /etc/apache2/certs/aggregator
  cp -pr security/cacert.pem security/bn.com/r1/cert.pem security/bn.com/r1/key.pem /etc/apache2/certs/registry
  cp -pr security/cacert.pem security/bn.com/ba1/cert.pem security/bn.com/ba1/key.pem /etc/apache2/certs/aggregator
  ```

2. Virtual host for the registry should contain in its setup next declarations (among others):

  ```apache
  SSLCACertificateFile     /etc/apache2/certs/registry/cacert.pem
  SSLCertificateFile       /etc/apache2/certs/registry/cert.pem
  SSLCertificateKeyFile    /etc/apache2/certs/registry/key.pem
  
  SSLVerifyClient optional
  SSLOptions +StdEnvVars +ExportCertData
  
  SSLCipherSuite ALL:!ADH:!EXPORT56:!SSLv2:RC4+RSA:+HIGH:+MEDIUM:+LOW:+TLSv1:+SSLv3:-EXP:+eNULL
  
  ProxyPreserveHost On
  AllowEncodedSlashes On
  #SSLProxyVerify None
  #SSLProxyCheckPeerCN Off
  
  <Location /beacon-network/registry >
      SSLRequireSSL
      <If "%{REQUEST_METHOD} == 'POST' || %{REQUEST_METHOD} == 'PUT' || %{REQUEST_METHOD} == 'DELETE'">
          SSLOptions           +FakeBasicAuth
          SSLVerifyClient require
          SSLVerifyDepth 2
          
          # This line would restrict to certificates with next CNs
	  SSLRequire  ( %{SSL_CLIENT_S_DN_CN} eq "ba1.bn.com"  ||  %{SSL_CLIENT_S_DN_CN} eq "r1.bn.com" )
	  
	  # If you disable previous line and enable this, you are
	  # allowing all the valid certificates being signed by this CA
	  # SSLRequire %{SSL_CLIENT_I_DN_CN} eq "bn.com"
      </If>
  
      ProxyPass ajp://127.0.0.1:8009/beacon-network/registry
      ProxyPassReverse ajp://127.0.0.1:8009/beacon-network/registry
  </Location>
  ```

3. Virtual host for the aggregator should contain in its setup next declarations (among others):

  ```apache
  SSLCACertificateFile     /etc/apache2/certs/aggregator/cacert.pem
  SSLCertificateFile       /etc/apache2/certs/aggregator/cert.pem
  SSLCertificateKeyFile    /etc/apache2/certs/aggregator/key.pem
  
  SSLVerifyClient optional
  SSLOptions +StdEnvVars +ExportCertData
  
  SSLCipherSuite ALL:!ADH:!EXPORT56:!SSLv2:RC4+RSA:+HIGH:+MEDIUM:+LOW:+TLSv1:+SSLv3:-EXP:+eNULL
  
  ProxyPreserveHost On
  AllowEncodedSlashes On
  #SSLProxyVerify None
  #SSLProxyCheckPeerCN Off
  
  <Location /beacon-network/aggregator >
      SSLRequireSSL
      <If "%{REQUEST_METHOD} == 'POST' || %{REQUEST_METHOD} == 'PUT' || %{REQUEST_METHOD} == 'DELETE'">
          SSLOptions           +FakeBasicAuth
          SSLVerifyClient require
          SSLVerifyDepth 2
          
          # This line, when enabled, would restrict to certificates with next CN
	  SSLRequire  %{SSL_CLIENT_S_DN_CN} eq "ba1.bn.com"
	  
	  # If you disable previous line and enable this, you are
	  # allowing all the valid certificates being signed by this CA
	  # SSLRequire %{SSL_CLIENT_I_DN_CN} eq "bn.com"
      </If>
  
      ProxyPass ajp://127.0.0.1:18009/beacon-network/aggregator
      ProxyPassReverse ajp://127.0.0.1:18009/beacon-network/aggregator
  </Location>
  ```
