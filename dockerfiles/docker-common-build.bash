#!/bin/bash

set -e -v

BEACON_PROJECT="$1"
BEACON_PROJECT_TAG="$2"
MVNW_TAG="$3"

DEPLOY_DIR="${JBOSS_HOME}/standalone/deployments/"

BEACON_PROJECT_PREFIX="${BEACON_PROJECT}-${BEACON_PROJECT_TAG}"
BEACON_PROJECT_DIR="${BEACON_PROJECT_PREFIX}"

MVN_PROJECT="maven-wrapper"
MVNW_PREFIX="${MVN_PROJECT}-${MVNW_TAG}"
MVNW_DIR="${MVN_PROJECT}-${MVNW_PREFIX}"

# First, fetch the code and uncompress it
cd /tmp
curl -L -O "https://gitlab.bsc.es/inb/ga4gh/${BEACON_PROJECT}/-/archive/${BEACON_PROJECT_TAG}/${BEACON_PROJECT_PREFIX}.tar.gz"

tar xzf "${BEACON_PROJECT_PREFIX}".tar.gz
cd "${BEACON_PROJECT_DIR}"

if [ ! -f mwnw ] ; then
	# Optional second, fetch mvnw, and copy it
	curl -L -O "https://github.com/takari/${MVN_PROJECT}/archive/${MVNW_PREFIX}.tar.gz"
	tar xzf "${MVNW_PREFIX}".tar.gz
	cp -dpr "${MVNW_DIR}"/mvnw "${MVNW_DIR}"/.mvn .
	rm -rf "${MVNW_DIR}"
fi

# Third, compile the code
./mvnw clean package

# Fourth, install it
for A in target/*.war ; do
	cp -p "$A" "$DEPLOY_DIR"/"$(basename "$A").tmp"
done

# Last, cleanup
rm -rf "${HOME}/.m2"
cd /tmp
rm -rf "${BEACON_PROJECT_PREFIX}"*
