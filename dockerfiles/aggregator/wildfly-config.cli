set BEACON_AGGREGATOR_KEYSTORE=${BEACON_AGGREGATOR_KEYSTORE}
set BEACON_AGGREGATOR_KEYSTORE_PASS=${BEACON_AGGREGATOR_KEYSTORE_PASS}
set BEACON_AGGREGATOR_TRUSTSTORE=${BEACON_AGGREGATOR_TRUSTSTORE}
set BEACON_AGGREGATOR_TRUSTSTORE_PASS=${BEACON_AGGREGATOR_TRUSTSTORE_PASS}
set BEACON_AGGREGATOR_KEYSTORE_KEY_PASS=${BEACON_AGGREGATOR_KEYSTORE_KEY_PASS}

connect 127.0.0.1

batch

# configure SSL keystore & truststore
/subsystem=elytron/key-store=BeaconNetworkKeyStore:add(path=$BEACON_AGGREGATOR_KEYSTORE,credential-reference={clear-text=$BEACON_AGGREGATOR_KEYSTORE_PASS},type=JKS)
/subsystem=elytron/key-store=BeaconNetworkTrustStore:add(path=$BEACON_AGGREGATOR_TRUSTSTORE,credential-reference={clear-text=$BEACON_AGGREGATOR_TRUSTSTORE_PASS},type=JKS)
/subsystem=elytron/key-manager=BeaconNetworkKeyManager:add(key-store=BeaconNetworkKeyStore,credential-reference={clear-text=$BEACON_AGGREGATOR_KEYSTORE_KEY_PASS})
/subsystem=elytron/trust-manager=BeaconNetworkTrustManager:add(key-store=BeaconNetworkTrustStore)
/subsystem=elytron/server-ssl-context=BeaconNetworkSSLContext:add(key-manager=BeaconNetworkKeyManager,trust-manager=BeaconNetworkTrustManager,protocols=["TLSv1.2"],need-client-auth=false,want-client-auth=true,authentication-optional=true)

# configure SSL security domain to inject 'BEACON_NETWORK' role for all authenticated SSL certificates

/subsystem=elytron/key-store-realm=KeyStoreRealm:add(key-store=BeaconNetworkTrustStore)
/subsystem=elytron/x500-attribute-principal-decoder=x500-decoder:add(attribute-name=CN)
/subsystem=elytron/constant-role-mapper=beacon-network-role-mapper:add(roles=[BEACON_NETWORK])
/subsystem=elytron/security-domain=BeaconNetworkDomain:add(default-realm=KeyStoreRealm,permission-mapper=default-permission-mapper,principal-decoder=x500-decoder,role-mapper=beacon-network-role-mapper,realms=[{realm=KeyStoreRealm,role-decoder=groups-to-roles}])

/subsystem=elytron/http-authentication-factory=beacon-network-authentication:add(security-domain=BeaconNetworkDomain,http-server-mechanism-factory=global,mechanism-configurations=[{mechanism-name=CLIENT_CERT}])
/subsystem=undertow/application-security-domain=beacon-network-domain:add(http-authentication-factory=beacon-network-authentication)

# configure ajp/http/https listeners

/subsystem=undertow/server=default-server/ajp-listener=ajp:add(socket-binding=ajp, scheme=https, redirect-socket=https, enabled=true)
/subsystem=undertow/server=default-server/http-listener=default:undefine-attribute(name=redirect-socket)
/subsystem=undertow/server=default-server/https-listener=https:undefine-attribute(name=security-realm)
/subsystem=undertow/server=default-server/https-listener=https:write-attribute(name=ssl-context,value=BeaconNetworkSSLContext)

run-batch

reload