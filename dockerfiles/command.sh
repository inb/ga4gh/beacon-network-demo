#!/bin/bash

set -e

# JBOSS_HOME envvar contains the path to Wildfly

if [ -n "$BEACON_PRODUCTION" ] ; then
	BEACON_DEBUG=
else
	BEACON_DEBUG="-Djavax.net.debug=all"
fi

if [ -z "$BEACON_KEYSTORE" ] ; then
	if [ -n "$BEACON_REGISTRY_KEYSTORE" ] ; then
		BEACON_KEYSTORE="${BEACON_REGISTRY_KEYSTORE}"
	elif [ -n "$BEACON_AGGREGATOR_KEYSTORE" ] ; then
		BEACON_KEYSTORE="${BEACON_AGGREGATOR_KEYSTORE}"
	else
		BEACON_KEYSTORE="/etc/ega/registry.keystore"
	fi
fi

if [ -z "$BEACON_KEYSTORE_PASS" ] ; then
	if [ -n "$BEACON_REGISTRY_KEYSTORE_PASS" ] ; then
		BEACON_KEYSTORE_PASS="${BEACON_REGISTRY_KEYSTORE_PASS}"
	elif [ -n "$BEACON_AGGREGATOR_KEYSTORE_PASS" ] ; then
		BEACON_KEYSTORE_PASS="${BEACON_AGGREGATOR_KEYSTORE_PASS}"
	else
		BEACON_KEYSTORE_PASS=changeme
	fi
fi

if [ -z "$BEACON_TRUSTSTORE" ] ; then
	if [ -n "$BEACON_REGISTRY_TRUSTSTORE" ] ; then
		BEACON_TRUSTSTORE="${BEACON_REGISTRY_TRUSTSTORE}"
	elif [ -n "$BEACON_AGGREGATOR_TRUSTSTORE" ] ; then
		BEACON_TRUSTSTORE="${BEACON_AGGREGATOR_TRUSTSTORE}"
	else
		BEACON_TRUSTSTORE="/etc/ega/registry.trust.keystore"
	fi
fi

if [ -z "$BEACON_TRUSTSTORE_PASS" ] ; then
	if [ -n "$BEACON_REGISTRY_TRUSTSTORE_PASS" ] ; then
		BEACON_TRUSTSTORE_PASS="${BEACON_REGISTRY_TRUSTSTORE_PASS}"
	elif [ -n "$BEACON_AGGREGATOR_TRUSTSTORE_PASS" ] ; then
		BEACON_TRUSTSTORE_PASS="${BEACON_AGGREGATOR_TRUSTSTORE_PASS}"
	else
		BEACON_TRUSTSTORE_PASS=changeme
	fi
fi

"${JBOSS_HOME}"/bin/standalone.sh ${BEACON_DEBUG} \
 "-Djavax.net.ssl.keyStore=${BEACON_KEYSTORE}" \
 "-Djavax.net.ssl.keyStorePassword=${BEACON_KEYSTORE_PASS}" \
 "-Djavax.net.ssl.trustStore=${BEACON_TRUSTSTORE}" \
 "-Djavax.net.ssl.trustStorePassword=${BEACON_TRUSTSTORE_PASS}" \
 -b 0.0.0.0 -bmanagement 127.0.0.1 &


if find "${JBOSS_HOME}"/standalone/deployments -type f -name "*.war.tmp" ; then

	B_STAFF="$(dirname "$0")"

	# Waiting for Wildfly administrative port
	while true
	do
	    sleep 1
	    if (echo >/dev/tcp/127.0.0.1/9990) &>/dev/null
		then break
	    fi
	done

	# Waiting for Wildfly ready state
	while true
	do
	    if "${JBOSS_HOME}"/bin/jboss-cli.sh --connect command=':read-attribute(name=server-state)' | grep -q running
		then break
	    fi
	    sleep 1
	done

	# Environment variables needed by jboss-cli scripting
	printenv > /tmp/beacon-setup.properties

	# Now, we are going to set it up
	"${JBOSS_HOME}"/bin/jboss-cli.sh --properties=/tmp/beacon-setup.properties --file="${B_STAFF}"/wildfly-config.cli

	rm -f /tmp/beacon-setup.properties

	# Doing the real deployment
	for WAR in "${JBOSS_HOME}"/standalone/deployments/*.war.tmp ; do
		mv "${WAR}" "$(dirname "${WAR}")/$(basename "${WAR}" .tmp)"
	done
fi

if [ -n "$BEACON_REGISTRY_CA_FILE" -a -f "$BEACON_REGISTRY_CA_FILE" -a -n "$BEACON_REGISTRY_INITIAL_BEACONS" -a -f "$BEACON_REGISTRY_INITIAL_BEACONS" ] ; then
	# wait Wildfly is up and running
	while true
	do
	    sleep 1
	    if (echo >/dev/tcp/127.0.0.1/8080) &>/dev/null
		then break
	    fi
	done

	# Checking the application has been properly deployed
	while true
	do
	    if  curl --fail -s http://127.0.0.1:8080/${BEACON_STUFF_ENDPOINT}/info
		then break
	    fi
	    sleep 1
	done
	
	# Populating the beacon registry
	curl --cacert "${BEACON_REGISTRY_CA_FILE}" --cert "${BEACON_REGISTRY_CLIENT_CERT}" --key "${BEACON_REGISTRY_CLIENT_KEY}" -v -X POST -H "Content-Type: application/json" https://$(hostname):8443/${BEACON_STUFF_ENDPOINT}/services -d @"$BEACON_REGISTRY_INITIAL_BEACONS"
fi

wait
