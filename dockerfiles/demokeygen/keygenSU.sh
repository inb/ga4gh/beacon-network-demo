#!/bin/sh

set -e

if [ $# -lt 3 ] ; then
	echo "Usage: $0 {destdir} {domainName} {keystorepass} {machine_names}*" 1>&2
	exit 1
fi

if [ -n "$OUTER_UID" -a -n "$OUTER_GID" ] ; then
	workdir="$1"
	shift
	
	tmpworkdir="$(mktemp -d)"
	trap "rm -rf $tmpworkdir" EXIT
	
	if [ -f "${workdir}/cacert.pem" ] ; then
		cp -dpr "$workdir"/* "$tmpworkdir"
	fi
	
		
	keygen.sh "$tmpworkdir" "$@"
	chown -R "$OUTER_UID:$OUTER_GID" "$tmpworkdir"
	cp -dpr "$tmpworkdir"/* "$workdir"
else
	echo "You need to pass OUTER_UID and OUTER_GID environment variables"
	#exec keygen.sh "$@"
fi
