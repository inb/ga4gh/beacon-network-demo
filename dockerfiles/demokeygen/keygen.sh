#!/bin/sh

set -e

echo "This tool will generate for a domain name a set of keys"

if [ $# -lt 3 ] ; then
	echo "Usage: $0 {destdir} {domainName} {keystorepass} {machine_names}*" 1>&2
	exit 1
fi

# These tools are needed
if which apk > /dev/null ; then
	for tool in keytool openssl certtool ; do
		which "$tool" > /dev/null
		if [ $? -ne 0 ] ; then
			apk add --update gnutls-utils openssl openjdk11-jre
			break
		fi
	done
fi

if [ -z "$JAVA_HOME" ] ; then
	for path in /usr/lib/jvm/default-jvm ; do
		if [ -d "$path" ] ; then
			JAVA_HOME="$path"
			export JAVA_HOME
		fi
	done
fi

DESTDIR="$1"
shift
domainName="$1"
shift
keystorePass="$1"
shift

mkdir -p "$DESTDIR"

workdir="$(mktemp -d)"
trap "rm -rf '$workdir'" EXIT


keystoreDir="${DESTDIR}/${domainName}"
keysDir="${keystoreDir}/keys"
CAcert="${keystoreDir}"/cacert.pem
CAkey="${keystoreDir}"/cakey.pem

if [ ! -f "${CAcert}" ] ; then
	mkdir -p "${keystoreDir}"
	chmod go= "${keystoreDir}"
	
	CA_TEMPLATE="${workdir}/ca-template.cfg"

	# First, the self-signed CA
	cat <<EOF > "$CA_TEMPLATE"
cn = "$domainName"
ca
cert_signing_key
expiration_days = 50000
EOF
	
	(umask 277 && certtool --generate-privkey --outfile "${CAkey}")
	certtool --generate-self-signed \
		--template "${CA_TEMPLATE}" \
		--load-privkey "${CAkey}" \
		--outfile "${CAcert}"
fi

# generation of the working JKS
tempKeystore="${workdir}/keystore.jks"
cp "${JAVA_HOME}"/jre/lib/security/cacerts "${tempKeystore}"
chmod u+w "${tempKeystore}"
keytool -storepasswd -new "${keystorePass}" -keystore "${tempKeystore}" -storepass changeit
keytool -v -alias 'root ca' -importcert -file "${CAcert}" -keystore "${tempKeystore}" -storepass "${keystorePass}" -noprompt -trustcacerts

if [ "$#" -gt 0 ] ; then
	# The keystores loop
	for certPair in "$@" ; do
		if [ -z "$certPair" ] ; then
			continue
		fi
		
		case "$certPair" in
			*=*)
				# It's both a server and a client certificate
				tls_www_server=tls_www_server
				tls_www_client=tls_www_client
				cert="${certPair%%=*}"
				;;
			*+*)
				# It's a server-only certificate
				tls_www_server=tls_www_server
				cert="${certPair%%+*}"
				;;
			*)
				# It's a client-only certificate
				tls_www_client=tls_www_client
				cert="${certPair}"
				;;
		esac
		
		dnsName="${cert}.${domainName}"
		
		certdir="${keystoreDir}/${cert}"
		if [ ! -f "${certdir}"/cert.pem ] ; then
			mkdir -p "${certdir}"

			certtemplate="${workdir}/${cert}.cfg"
			cat <<EOF > "$certtemplate"
# RD-Connect User Management Interface certificate options

# X.509 Certificate options
#
# DN options

# The common name of the certificate owner.
cn = ${cert}.${domainName}
# Organization
organization = "$cert at $domainName"
# Organizational unit
unit = "$cert at $domainName"
# A dnsname in case of a WWW server.
dns_name = ${dnsName}
# A subject alternative name URI
uri = https://${dnsName}/
uri = https://${dnsName}:8443/
# Locality
locality = Barcelona
# The state of the certificate owner.
state = Catalonia
# The country of the subject. Two letter code.
country = ES
# In how many days, counting from today, this certificate will expire.
# Use -1 if there is no expiration date.
expiration_days = 50000

#### Key usage

# The following key usage flags are used by CAs and end certificates

# Whether this certificate will be used to sign data (needed
# in TLS DHE ciphersuites). This is the digitalSignature flag
# in RFC5280 terminology.
signing_key

# Whether this certificate will be used to encrypt data (needed
# in TLS RSA ciphersuites). Note that it is preferred to use different
# keys for encryption and signing. This is the keyEncipherment flag
# in RFC5280 terminology.
encryption_key

# Whether this key will be used to sign other certificates. The
# keyCertSign flag in RFC5280 terminology.
#cert_signing_key

# Whether this key will be used to sign CRLs. The
# cRLSign flag in RFC5280 terminology.
#crl_signing_key

# The keyAgreement flag of RFC5280. It's purpose is loosely
# defined. Not use it unless required by a protocol.
#key_agreement

# The dataEncipherment flag of RFC5280. It's purpose is loosely
# defined. Not use it unless required by a protocol.
#data_encipherment

# The nonRepudiation flag of RFC5280. It's purpose is loosely
# defined. Not use it unless required by a protocol.
#non_repudiation

#### Extended key usage (key purposes)

# The following extensions are used in an end certificate
# to clarify its purpose. Some CAs also use it to indicate
# the types of certificates they are purposed to sign.


# Whether this certificate will be used for a TLS client;
# this sets the id-kp-serverAuth (1.3.6.1.5.5.7.3.1) of 
# extended key usage.
$tls_www_client

# Whether this certificate will be used for a TLS server;
# This sets the id-kp-clientAuth (1.3.6.1.5.5.7.3.2) of 
# extended key usage.
$tls_www_server

# Whether this key will be used to sign code. This sets the
# id-kp-codeSigning (1.3.6.1.5.5.7.3.3) of extended key usage
# extension.
#code_signing_key

# Whether this key will be used to sign OCSP data. This sets the
# id-kp-OCSPSigning (1.3.6.1.5.5.7.3.9) of extended key usage extension.
#ocsp_signing_key

# Whether this key will be used for time stamping. This sets the
# id-kp-timeStamping (1.3.6.1.5.5.7.3.8) of extended key usage extension.
#time_stamping_key

# Whether this key will be used for email protection. This sets the
# id-kp-emailProtection (1.3.6.1.5.5.7.3.4) of extended key usage extension.
#email_protection_key

# Whether this key will be used for IPsec IKE operations (1.3.6.1.5.5.7.3.17).
#ipsec_ike_key
EOF
			
			# First, generate private key
			certtool --generate-privkey --outfile "${certdir}"/key.pem
			
			# Give it read permissions in order to avoid future problems
			chmod ugo+r,u-w "${certdir}"/key.pem
			
			# Second, generate the public key, based on the profile
			certtool --generate-certificate \
				--template "${certtemplate}" \
				--load-privkey "${certdir}"/key.pem \
				--outfile "${certdir}"/cert.pem \
				--load-ca-certificate "${CAcert}" \
				--load-ca-privkey "${CAkey}"
		fi
		
		certpass="$keystorePass"
		# Last, generate a p12 keystore, which can be imported into a Java keystore
		initialCerttoolP12Keystore="${certdir}"/keystoreCerttool.p12
		if [ ! -f "${initialCerttoolP12Keystore}" ] ; then
			certtool --load-ca-certificate "${CAcert}" \
				--load-certificate "${certdir}"/cert.pem --load-privkey "${certdir}"/key.pem \
				--to-p12 --p12-name="${dnsName}" --password="${certpass}" --outder --outfile "${initialCerttoolP12Keystore}"
		fi
		
		initialOpenSSLP12Keystore="${certdir}"/keystoreOpenSSL.p12
		if [ ! -f "${initialOpenSSLP12Keystore}" ] ; then
			export certpass
			openssl pkcs12 -export -in "${certdir}"/cert.pem -inkey "${certdir}"/key.pem -name "${dnsName}" \
				 -certfile "${CAcert}" -password env:certpass -out "${initialOpenSSLP12Keystore}"
		fi
		
		#initialP12Keystore="${initialOpenSSLP12Keystore}"
		initialP12Keystore="${initialCerttoolP12Keystore}"
		
		keystoreFile="${certdir}/keystore.jks"
		# Does the truststore file already exist?
		if [ ! -f "$keystoreFile" ] ; then
			cp -p "${tempKeystore}" "$keystoreFile"
		fi
		
		keytool -v -importkeystore -srckeystore "${initialP12Keystore}" -srcstorepass "${certpass}" -srcstoretype PKCS12 \
			-destkeystore "${keystoreFile}" -deststorepass "${keystorePass}"
		
		if [ "$certpass" != "$keystorePass" ] ; then
			keyAlias="$(keytool -rfc -list -storetype PKCS12 -keystore "${initialP12Keystore}" -storepass "${certpass}" | grep -F 'Alias name' | head -n 1 | sed 's#^[^:]\+: \(.\+\)$#\1#')"
			keytool -keypasswd -keypass "${certpass}" -new "${keystorePass}" -alias "${keyAlias}" -keystore "${keystoreFile}" -storepass "${keystorePass}"
		fi
	done
	
	# The truststores loop
	for certPair in "$@" ; do
		if [ -z "$certPair" ] ; then
			continue
		fi
		
		case "$certPair" in
			*=*)
				# It's both a server and a client certificate
				cert="${certPair%%=*}"
				truststoreElems="${certPair#*=}"
				;;
			*+*)
				# It's a server-only certificate
				cert="${certPair%%+*}"
				truststoreElems="${certPair#*+}"
				;;
			*)
				# The client does not need a separate truststore
				continue
				;;
		esac
		
		certdir="${keystoreDir}/${cert}"
		truststoreFile="${certdir}/truststore.jks"
		# Does the truststore file already exist?
		#if [ ! -f "$truststoreFile" ] ; then
		#	cp -p "${tempKeystore}" "$truststoreFile"
		#fi
		if [ -f "$truststoreFile" ] ; then
			rm -f "$truststoreFile"
		fi
		keytool -v -alias 'root ca' -importcert -file "${CAcert}" -keystore "$truststoreFile" -storepass "${keystorePass}" -noprompt -trustcacerts
		
		if [ -n "$truststoreElems" ] ; then
			echo "$truststoreElems" | tr ',' '\n' | while IFS="," read -r trusthost ; do
				trustdir="${keystoreDir}/${trusthost}"
				trustPKPEM="${trustdir}/cert.pem"
				
				if [ ! -f "$trustPKPEM" ] ; then
					echo "ERROR: No certificates were found for ${trusthost}. Exit" 1>&2
					exit 1
				fi
				cnLine="$(openssl x509 -noout -subject -in "$trustPKPEM" -nameopt multiline | grep commonName)"
				# The alias name must be equals to the CN
				aliasName="${cnLine#*= }"
				
				keytool -import -alias "$aliasName" -file "$trustPKPEM" -keystore "$truststoreFile" -storepass "${keystorePass}" -noprompt -trustcacerts
			done
		fi
	done
fi

echo "JKS Keystores, truststores and all the self signed certificates are available at $DESTDIR"

trap - EXIT

exit 0
